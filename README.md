# README #

This is a Unity simulation of a particle filter localization. The robot uses 8 distance sensors to navigate on a randomly generated map.
It is built with Unity 5.3.p4.

Learn more abour PFL on wiki: https://en.wikipedia.org/wiki/Monte_Carlo_localization