﻿using UnityEngine;
using System.Collections;

public class ArenaBuilder : MonoBehaviour {

    public float fillRate = 0.1f;
    public int robotClearance = 3;

	static public int width = 18;
    static public int height = 18;

    public bool[,] map = new bool[height, width / 2];

    // Initialization
    public void Awake ()
    {
        StartCoroutine(Reset());
    }

    // Create brand new world
    IEnumerator Reset ()
    {
        AnalyticsManager.SendRestartEvent(Robot.numberOfParticles);
        Status.SetStatus("Generating map...");
        yield return new WaitForEndOfFrame();
        ClearMap();
        yield return new WaitForEndOfFrame();
        GenerateMap();
        Instantiate(Resources.Load<GameObject>("Robot"), new Vector3(0, 0.4f, 0), new Quaternion(0, 0, 0, 0));
    }

    // Remove all particles and walls
    private void ClearMap()
    {
        GameObject[] robots = GameObject.FindGameObjectsWithTag("Robot");
        foreach (var r in robots)
        {
            if (r != null)
            {
                try
                {
                    r.GetComponent<Robot>().CancelInvoke();
                    GameObject.DestroyImmediate(r);
                }
                catch { }
            }
        }
        foreach (var go in GameObject.FindGameObjectsWithTag("Particle"))
        {
            MMFactory.instance.Remove(go.GetComponent<MotionModel>());
        }
        foreach (var go in GameObject.FindGameObjectsWithTag("Obstacle"))
        {
            GameObject.Destroy(go);
        }
        // Add random walls to the map
        for (int i = 0; i < height; i++)
            for (int j = 0; j < width / 2; j++)
                map[i, j] = false;
    }

    // Create a random map
    private void GenerateMap()
    {
        // Add random walls to the map
        for (int i = 0; i < height; i++)
            for (int j = 0; j < width / 2; j++)
            {
                bool startLine = (Random.Range(0, 1 / fillRate) < 1);
                if (startLine)
                {
                    bool isVertical = (Random.Range(0, 2) < 1);
                    int len = (int)Random.Range(0, width / 3);
                    for (int m = 0; m < len; m++)
                    {
                        if (isVertical && (i + m < height))
                            map[i + m, j] = true;
                        if (!isVertical && (j + m < width / 2))
                            map[i, j + m] = true;
                    }
                }
            }

        // Erase area around robot
        for (int i = height / 2 - robotClearance; i < height / 2 + robotClearance; i++)
            for (int j = width / 2 - robotClearance; j < width / 2; j++)
            {
                map[i, j] = false;
            }

        // Add borders
        for (int i = 0; i < height; i++)
        {
            map[i, 0] = true;
        }
        for (int j = 0; j < width / 2; j++)
        {
            map[0, j] = true;
            map[height - 1, j] = true;
        }

        // Create floor
        GameObject floor;
        floor = Instantiate(Resources.Load<GameObject>("Floor"), new Vector3(0, 0, 0.5f), new Quaternion(0, 0, 0, 0)) as GameObject;
        floor.transform.localScale = new Vector3((float)width / 10, 1, (float)height / 10);

        // Instantiate the walls
        for (int i = 0; i < height; i++)
            for (int j = 0; j < width / 2; j++)
            {
                if (map[i, j])
                {
                    Vector3 pos = new Vector3((width / 2 - j - 0.5f), 0.4f, (height / 2 - i));
                    Instantiate(Resources.Load<GameObject>("Wall"), pos, new Quaternion(0, 0, 0, 0));
                    pos = new Vector3((j - width / 2 + 0.5f), 0.4f, (height / 2 - i));
                    Instantiate(Resources.Load<GameObject>("Wall"), pos, new Quaternion(0, 0, 0, 0));
                }
            }
    }
}
