﻿using System;

public sealed class GaussianRandom
{
    private bool _hasDeviate;
    private float _storedDeviate;
    private readonly Random _random;

    public GaussianRandom()
    {
        _random = new Random();
    }

    // Get random values with mean mu and variance sigma
    public float NextGaussian(float mu = 0, float sigma = 1)
    {
        if (sigma <= 0)
            throw new ArgumentOutOfRangeException("sigma", "Must be greater than zero.");

        if (_hasDeviate)
        {
            _hasDeviate = false;
            return _storedDeviate * sigma + mu;
        }

        float v1, v2, rSquared;
        do
        {
            // Two random values between -1.0 and 1.0
            v1 = (float) (2 * _random.NextDouble() - 1);
            v2 = (float) (2 * _random.NextDouble() - 1);
            rSquared = v1 * v1 + v2 * v2;
        } while (rSquared >= 1 || rSquared == 0);

        // Calculate polar tranformation for each deviate
        float polar = (float)Math.Sqrt(-2 * Math.Log(rSquared) / rSquared);

        // Store first deviate
        _storedDeviate = v2 * polar;
        _hasDeviate = true;

        // Return second deviate
        return v1 * polar * sigma + mu;
    }
}