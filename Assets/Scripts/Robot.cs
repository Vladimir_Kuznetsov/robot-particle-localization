﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Robot : MotionModel
{

    public float filterAccuracy;

    // Performance depending variables
    public float moveAccuracy = 0.5f;
    public float rotationAccuracy = 0.5f;
    public int CHECK_TIME_AFTER_THIS_NUMBER_OF_PARTICLES = 100;
    
    public static int numberOfParticles = 100;

    List<MotionModel> particles = new List<MotionModel>();


    public float sensorNoise = 0.05f;
    public float moveSpeed = 3;
    public float rotationSpeed = 5;

    public Transform sensor12;
    public Transform sensor34;
    public Transform sensor56;
    public Transform sensor78;

    bool IsPauseTime (int i)
    {
        if (CHECK_TIME_AFTER_THIS_NUMBER_OF_PARTICLES == 0 || 
            i % CHECK_TIME_AFTER_THIS_NUMBER_OF_PARTICLES == 0)
        {
            return true;
        }
        return false;
    }
    
    // Use this for initialization
    void Start()
    {
        base.Start();
        GenerateParticles();
        StartCoroutine(SamplingLoop());
    }

    // Control the robot
    void FixedUpdate()
    {
        float m = Input.GetAxis("Vertical") * moveSpeed;
        float r = Input.GetAxis("Horizontal") * rotationSpeed;
        MoveAll(m, r);
    }

    // Accurate drawings
    void LateUpdate()
    {
        UpdateSensorVisualization();
    }

    // Update particles
    IEnumerator SamplingLoop()
    {
        yield return new WaitForEndOfFrame();

        SensorData robotSense = Sense(sensorNoise);

        while (true)
        {
            System.DateTime newSamplingCycle = System.DateTime.Now;

            // Update particles sensor data
            Status.SetStatus("Estimate particles weights");
            for (int i = 0; i < particles.Count; i++)
            {
                robotSense = Sense(sensorNoise);
                particles[i].UpdateWeight(robotSense, filterAccuracy, sensorNoise);
                if (IsPauseTime(i))
                {
                    yield return null;
                    robotSense = Sense(sensorNoise);
                }
            }
            yield return null;

            // Create cumulative list
            Status.SetStatus("Creating cumulative list");
            List<float> cumulativeWeights = new List<float>();
            float maxParticleWeight = 0;
            Vector3 maxParticlePos = Vector3.zero;

            cumulativeWeights.Add(particles[0].weight);

            for (int i = 1; i < particles.Count; i++)
            {
                // Search for the closest particle - we're gonna need it
                if (particles[i].weight > maxParticleWeight)
                {
                    maxParticleWeight = particles[i].weight;
                    maxParticlePos = particles[i].transform.localPosition;
                }

                cumulativeWeights.Add(cumulativeWeights[i - 1] + particles[i].weight);
                if (IsPauseTime(i))
                {
                    yield return null;
                }
            }

            if (cumulativeWeights[cumulativeWeights.Count - 1] == 0)
            {
                Debug.Log("All particles have zero weight.");
            }

            // Add chance of generating a random particle
            float fine = GetFineWeight(filterAccuracy);
            cumulativeWeights.Add(cumulativeWeights[cumulativeWeights.Count - 1] + fine);

            yield return null;

            // Resample particle list
            Status.SetStatus("Resampling in progress");
            int oldNumberOfParticles = numberOfParticles;

            float step = cumulativeWeights[cumulativeWeights.Count - 1] / oldNumberOfParticles;
            float beta = step;

            for (int i = 0; i < oldNumberOfParticles; i++)
            {
                while (cumulativeWeights.Count > 1 && cumulativeWeights[0] < beta)
                {
                    cumulativeWeights.RemoveAt(0);
                    MMFactory.instance.Remove(particles[0]);
                    particles.RemoveAt(0);
                }
                if (cumulativeWeights.Count == 1)
                {
                    // Some magic to calculate apropriate variance for Gaussian random function
                    float variance = Mathf.Clamp(fine * 3 - maxParticleWeight, 0, fine * 3) / (fine * 3);
                    variance = ArenaBuilder.width * Mathf.Clamp(variance, 0.01f, 1);
                    particles.Add(MMFactory.instance.RandomParticle(maxParticlePos, variance));
                }
                else
                {
                    particles.Add(MMFactory.instance.Create(particles[0]));
                }
                beta += step;

                if (IsPauseTime(i))
                {
                    Status.SetStatus("Resampling in progress: " + i);
                    yield return null;
                }
            }
            yield return null;
            /*
            // Hide old particles
            Status.SetStatus("Hiding old particles");
            MMFactory.instance.Remove(particles.GetRange(0, oldNumberOfParticles));
            particles.RemoveRange(0, oldNumberOfParticles);
            yield return null;
            */
            // Update performance parameters
            UpdatePerformance((int)(System.DateTime.Now - newSamplingCycle).TotalMilliseconds);
            yield return null;
        }
    }

    // Move every particle
    void MoveAll(float m, float r)
    {
        Move(m, r);
        foreach (MotionModel particle in particles)
        {
            if (particle.gameObject.activeSelf)
            {
                particle.Move(m + Random.Range(-moveAccuracy, moveAccuracy) * moveSpeed,
                    r + Random.Range(-rotationAccuracy, rotationAccuracy) * rotationSpeed);
            }
        }
    }

    // Initial particle generation
    void GenerateParticles()
    {
        for (int i = 0; i < numberOfParticles; i++)
        {
            particles.Add(MMFactory.instance.RandomParticle());
        }
    }

    // Update parameters based on simulation performance
    void UpdatePerformance(int millis)
    {
        if (millis > 0)
        {
            MotionModel.FILTER_K = Mathf.Clamp(-millis * 0.0007f + 0.5f, 0.05f, 0.5f);

            if (millis > 500)
            {
                CHECK_TIME_AFTER_THIS_NUMBER_OF_PARTICLES += numberOfParticles / 10;
            } else
            {
                CHECK_TIME_AFTER_THIS_NUMBER_OF_PARTICLES -= numberOfParticles / 10;
            }
            CHECK_TIME_AFTER_THIS_NUMBER_OF_PARTICLES = Mathf.Clamp(CHECK_TIME_AFTER_THIS_NUMBER_OF_PARTICLES,
                                                                    numberOfParticles / 5, numberOfParticles + 1);
        }
    }

    // Draw sensor axis
    public void UpdateSensorVisualization ()
    {
        SensorData s = Sense(sensorNoise);
        DrawSensor(ref sensor12, s.f, s.b);
        DrawSensor(ref sensor34, s.fr, s.bl);
        DrawSensor(ref sensor56, s.r, s.l);
        DrawSensor(ref sensor78, s.br, s.fl);
    }

    // Draw single sensor axis
    public void DrawSensor (ref Transform sensor, float d1, float d2)
    {
        if (d1 < 0 || d2 < 0 || sensor == null) return;
        sensor.localScale = new Vector3((d1 + d2) / transform.localScale.x, 
                                        .1f / transform.localScale.y, 
                                        .1f / transform.localScale.z);
        float mid =  d1 - (d1 + d2) / 2;
        float rot = sensor.localRotation.eulerAngles.y / 57.296f;
        sensor.localPosition = new Vector3(-mid * Mathf.Cos(rot), 0, mid * Mathf.Sin(rot));
    }
}
