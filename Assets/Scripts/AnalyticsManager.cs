﻿using UnityEngine;
using UnityEngine.Analytics;
using System.Collections;
using System.Collections.Generic;

public static class AnalyticsManager {

	public static void SendRestartEvent (int particles)
    {
        Analytics.CustomEvent("restart", new Dictionary<string, object>
        {
            { "particles", particles }
        });
    }
}
