﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody))]
public class MotionModel : MonoBehaviour { 

    Rigidbody _body;
    Rigidbody body
    {
        get
        {
            if (_body == null)
                _body = GetComponent<Rigidbody>();
            return _body;
        }
    }
    LayerMask ignoreMask;

    SpriteRenderer sp;

    public static float FILTER_K = 0;

    public bool isActive = false;

    public float weight;
    public int collisions = 0;

    public bool checkPosition
    {
        get
        {
            try {
                bool isOk = transform.position.x > -ArenaBuilder.width / 2 &&
                               transform.position.x < ArenaBuilder.width / 2 &&
                               transform.position.z < ArenaBuilder.height / 2 &&
                               transform.position.z > -ArenaBuilder.height / 2;
                return isOk;
            } catch
            {
                return false;
            }
        }
    }

    private Color roughColor = new Color(0.5f, 0.5f, 0.5f);
    private Color fineColor = new Color(1f, 0.2f, 0.2f);

    public static float GetFineWeight(float accuracy)
    {
        float res = 1 / Mathf.Pow(1.5f, accuracy);
        return res;
    }

    public void Awake()
    {
        sp = GetComponentInChildren<SpriteRenderer>();
    }

    public void Start()
    {
        int particleLayer = LayerMask.NameToLayer("Obstacle");
        ignoreMask = (1 << particleLayer);
    }

    public void Reset(float _weight = -1)
    {
        collisions = 0;
        weight = _weight;
        isActive = true;
    }

    public void Move(float m, float r)
    {
        if (m == 0 && r == 0)
            body.angularVelocity = new Vector3(body.angularVelocity.x, 0, body.angularVelocity.z);

        body.MoveRotation(transform.rotation * Quaternion.Euler(0, (m >= 0? r : -r), 0));
        body.MovePosition(transform.position + transform.forward * m);
    }

    public SensorData Sense(float noise)
    {
        var d = new SensorData();
        
        RaycastHit hit;
        Physics.Raycast(transform.position, transform.TransformDirection(Vector3.forward), out hit, 999, ignoreMask);
        d.f = hit.distance;
        Physics.Raycast(transform.position, transform.TransformDirection(Vector3.back), out hit, 999, ignoreMask);
        d.b = hit.distance;
        Physics.Raycast(transform.position, transform.TransformDirection(Vector3.left), out hit, 999, ignoreMask);
        d.l = hit.distance;
        Physics.Raycast(transform.position, transform.TransformDirection(Vector3.right), out hit, 999, ignoreMask);
        d.r = hit.distance;

        Physics.Raycast(transform.position, transform.TransformDirection(Vector3.right + Vector3.forward), out hit, 999, ignoreMask);
        d.fr = hit.distance;
        Physics.Raycast(transform.position, transform.TransformDirection(Vector3.left + Vector3.forward), out hit, 999, ignoreMask);
        d.fl = hit.distance;
        Physics.Raycast(transform.position, transform.TransformDirection(Vector3.right + Vector3.back), out hit, 999, ignoreMask);
        d.br = hit.distance;
        Physics.Raycast(transform.position, transform.TransformDirection(Vector3.left + Vector3.back), out hit, 999, ignoreMask);
        d.bl = hit.distance;

        d.f *= Random.Range(1 - noise, 1 + noise);
        d.b *= Random.Range(1 - noise, 1 + noise);
        d.l *= Random.Range(1 - noise, 1 + noise);
        d.r *= Random.Range(1 - noise, 1 + noise);

        d.fr *= Random.Range(1 - noise, 1 + noise);
        d.fl *= Random.Range(1 - noise, 1 + noise);
        d.br *= Random.Range(1 - noise, 1 + noise);
        d.bl *= Random.Range(1 - noise, 1 + noise);

        return d;
    }

    public void UpdateWeight(SensorData s1, float filterAccuracy, float noise)
    {
        SensorData s2 = Sense(noise);
        if (s2 != null)
        {
            float newWeight = 1 / Mathf.Max(Mathf.Abs(Mathf.Pow(s1.f - s2.f, filterAccuracy)) +
                Mathf.Abs(Mathf.Pow(s1.b - s2.b, filterAccuracy)) +
                Mathf.Abs(Mathf.Pow(s1.l - s2.l, filterAccuracy)) +
                Mathf.Abs(Mathf.Pow(s1.r - s2.r, filterAccuracy)) +
                Mathf.Abs(Mathf.Pow(s1.fr - s2.fr, filterAccuracy)) +
                Mathf.Abs(Mathf.Pow(s1.fl - s2.fl, filterAccuracy)) +
                Mathf.Abs(Mathf.Pow(s1.br - s2.br, filterAccuracy)) +
                Mathf.Abs(Mathf.Pow(s1.bl - s2.bl, filterAccuracy)), 0.000001f);

            weight = (weight == -1) ? newWeight : newWeight * (1 - FILTER_K) + weight * FILTER_K;
            weight = checkPosition ? weight : 0;
        } else
        {
            weight = 0;
        }
        
        if (weight < GetFineWeight(filterAccuracy) * 5) sp.color = roughColor;
        else sp.color = fineColor;
    }

    void OnCollisionEnter(Collision collision)
    {
        collisions++;
    }


    void OnCollisionExit(Collision collision)
    {
        collisions--;
    }
}
