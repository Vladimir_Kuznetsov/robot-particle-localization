﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class FaceCamera : MonoBehaviour {

    void LateUpdate()
    {
        if (Camera.current != null)
        {
            transform.LookAt(Camera.current.transform.position, -Vector3.up);
        }
    }
}
