﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Status : MonoBehaviour {

    static Text t;

	// Use this for initialization
	void Start () {
        t = GetComponent<Text>();
	}

    // Print the status
    public static bool SetStatus (string s)
    {
        if (t == null) return false;

        t.text = s;
        return true;
    }
}
