﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MMFactory : MonoBehaviour {

    static Vector3 poolPos = new Vector3(1000, 1000, 1000);

    static Stack<MotionModel> pool = new Stack<MotionModel> ();

    GaussianRandom rand;

    static MMFactory _instance;

    public static MMFactory instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = GameObject.FindObjectOfType<MMFactory>();
            }
            return _instance;
        }
    }

    // Get new MM object from pull or instantiate it
    public MotionModel Create(Vector3 pos, Quaternion rot, float weight = -1)
    {
        MotionModel mm = null;
        try {
            if (pool.Count > 0)
            {
                mm = pool.Pop();
                mm.transform.position = pos;
                mm.transform.rotation = rot;
            } else
            {
                mm = (Instantiate(Resources.Load<GameObject>("Particle"), pos, rot) as GameObject).GetComponent<MotionModel>();
            }
            mm.Reset(weight);
        } catch { }
        return mm;
    }

    // Get new MM object from pull or instantiate it
    public MotionModel Create(MotionModel mm)
    {
        if (mm != null)
        {
            return Create(mm.transform.position, mm.transform.rotation, mm.weight);
        }
        else
        {
            return RandomParticle();
        }
    }

    // Send MM object to the pull
    public void Remove (MotionModel mm)
    {
        try {
            mm.isActive = false;

            pool.Push(mm);
            mm.transform.position = poolPos;
        } catch { }
    }

    // Push particle to the pull and hide it
    public void Remove(List<MotionModel> lm)
    {
        try {
            foreach (MotionModel m in lm)
            {
                Remove(m);
            }
        } catch { }
    }

    // Create a single random particle
    public MotionModel RandomParticle(Vector3 mean = default(Vector3), float variance = 100)
    {
        if (rand == null)
            rand = new GaussianRandom();
        float x, z;
        if (variance > ArenaBuilder.width * 2)
        {
            x = Random.Range(-ArenaBuilder.width / 2 + 1.5f, ArenaBuilder.width / 2 - 1.5f);
            z = Random.Range(-ArenaBuilder.height / 2 + 1.5f, ArenaBuilder.height / 2 - 1.5f);
        } else { 
            x = rand.NextGaussian(mean.x, variance) % (ArenaBuilder.width / 2);
            z = rand.NextGaussian(mean.z, variance) % (ArenaBuilder.height / 2);
        }
        
        
        float r = Random.Range(0, 360);
        return Create(new Vector3(x, 0.4f, z), Quaternion.Euler(0, r, 0));
    }
}
