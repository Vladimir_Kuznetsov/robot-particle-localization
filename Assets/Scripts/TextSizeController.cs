﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class TextSizeController : MonoBehaviour {

    public Text[] texts;
    private int height = 0;

	// Use this for initialization
	void Start () {
        texts = transform.GetComponentsInChildren<Text>();
	}
	
	// Update is called once per frame
	void Update () {
	    if (Screen.height != height)
        {
            height = Screen.height;
            foreach (var t in texts)
            {
                t.fontSize = Mathf.Clamp(height / 30, 12, 36);
            }
        }
	}
}
