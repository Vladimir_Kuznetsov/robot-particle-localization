﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class DigitalInput : MonoBehaviour {

    public InputField f;
    private int val = 100;
    public int min = 20;
    public int max = 200;

    void Awake ()
    {
        f = GetComponent<InputField>();
        f.onEndEdit.AddListener(OnTextChanged);
        int.TryParse(f.text, out val);
    }

    public void OnTextChanged (string input)
    {
        int i_val;
        if (int.TryParse(input, out i_val))
        {
            if (i_val < min)
            {
                i_val = min;
            }
            if (i_val > max)
            {
                i_val = max;
            }
            Robot.numberOfParticles = i_val;
        } else
        {
            i_val = val;
        }
        try {
            f.text = i_val.ToString();
        } catch {  }
    }
}
